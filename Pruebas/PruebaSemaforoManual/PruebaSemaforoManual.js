
//http://192.168.7.2/user/Pruebas/PruebaSemaforoManual/PruebaSemaforoManual.html

setTargetAddress(location.host, {
  initialized: run
  }); //Apuntamos a la direccion y ejecutamos en funcion run


let b = undefined;

let ledPin1 = "P8_8"; //led verde
let ledPin2 = "P8_10";//led amarillo
let ledPin3 = "P8_12"; //led rojo


function run(){

  b = myrequire('bonescript'); //libreria Bonescript
  b.pinMode(ledPin1, b.OUTPUT);
  b.pinMode(ledPin2, b.OUTPUT);
  b.pinMode(ledPin3, b.OUTPUT);
}

function botonVerde(){

  b.digitalWrite(ledPin1, b.HIGH);
  b.digitalWrite(ledPin2, b.LOW);
  b.digitalWrite(ledPin3, b.LOW);
}

function botonAmarillo(){

  b.digitalWrite(ledPin1, b.LOW);
  b.digitalWrite(ledPin2, b.HIGH);
  b.digitalWrite(ledPin3, b.LOW);
}

function botonRojo(){

  b.digitalWrite(ledPin1, b.LOW);
  b.digitalWrite(ledPin2, b.LOW);
  b.digitalWrite(ledPin3, b.HIGH);
}