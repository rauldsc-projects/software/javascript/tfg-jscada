//http://192.168.7.2/user/Pruebas/PruebaLDR/PruebaLDR.html

setTargetAddress(location.host, {
  initialized: run
  }); //Apuntamos a la direccion y ejecutamos en funcion run



let b = undefined;

let ldrPin = "P9_36";
let ledPin1 = "P8_8";
let ledPin2 = "P8_10";
let ledPin3 = "P8_12";
let resultados = 
  [ 0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,]; //Vector necesario para construir la grafica

function run(){
  b = myrequire('bonescript'); //libreria Bonescript
  b.pinMode(ledPin1, b.OUTPUT);
  b.pinMode(ledPin2, b.OUTPUT);
  b.pinMode(ledPin3, b.OUTPUT);
}

function ldrRead(){
  b.analogRead(ldrPin, ldrWrite);
  setTimeout(ldrRead, 1000);
}


function ldrWrite(x) {
  b.digitalWrite(ledPin1, b.LOW);
  b.digitalWrite(ledPin2, b.LOW);
  b.digitalWrite(ledPin3, b.LOW);
  resultados.shift();                   //Aqui actualizamos el vector
  resultados.push(Math.round(x.value * 100)); 
  if(x.value <= (33.33/100)){
      b.digitalWrite(ledPin3, b.HIGH);
  }
  if(x.value > (33.33/100) && x.value <= (66.66/100)){
      b.digitalWrite(ledPin2, b.HIGH);
  }
  if(x.value > (66.66/100)){
      b.digitalWrite(ledPin1, b.HIGH);
  }
}

Highcharts.chart('container', {
chart: {
  type: 'spline',
  animation: Highcharts.svg, // don't animate in old IE (NO INTERNET EXPLORER!!)
  marginRight: 10,
  events: {
    load: function () {

      // Funcion que actualiza el grafico a cada segundo
      var series = this.series[0];
      setInterval(function () {
        var x = (new Date()).getTime(), // Aqui registramos la hora actual 
          y = resultados[49];               // Pasamos la ultima posicion del vector (la mas actualizada)
        series.addPoint([x, y], true, true);
      }, 1000);
    }
  }
},

time: {
  useUTC: false
},

title: {
  text: 'Live LDR data'
},
xAxis: {
  type: 'datetime',
  tickPixelInterval: 150
},
yAxis: {
  title: {
    text: 'Value'
  },
  plotLines: [{
    value: 0,
    width: 1,
    color: '#808080'
  }]
},
tooltip: {
  headerFormat: '<b>{series.name}</b><br/>',
  pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
},
legend: {
  enabled: false
},
exporting: {
  enabled: true
},
series: [{
  name: 'LDR value',
  data: (function () {
    // Generamos un vector que vaya guardando los datos anteriores
    var data = [],
      time = (new Date()).getTime(),
      i;

    for (i = -49; i <= 0; i += 1) {
      data.push({
        x: time + i * 1000,
        y: resultados[i + 49]
      });
    }
    return data;
  }())
}]
});
