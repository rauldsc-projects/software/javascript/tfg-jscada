//http://192.168.7.2/user/Pruebas/PruebaWeb/PruebaWeb.html

setTargetAddress(location.host, {
    initialized: run
    }); //Apuntamos a la direccion y ejecutamos en funcion run
  
  
  
  let b = undefined;
  
  let potPin = "P9_36";
  let ledPin1 = "P8_8"; //led verde
  let ledPin2 = "P8_10";//led amarillo
  let ledPin3 = "P8_12"; //led rojo
  
  let arrayY = [0]; //Vector necesario para construir la grafica

  function run(){
  
    b = myrequire('bonescript'); //libreria Bonescript
    b.pinMode(ledPin1, b.OUTPUT);
    b.pinMode(ledPin2, b.OUTPUT);
    b.pinMode(ledPin3, b.OUTPUT);
     
  }

  function ldrRead(){
    b.analogRead(potPin, ldrWrite);
    setTimeout(ldrRead, 100);
  }

  function ldrWrite(x) {
    b.digitalWrite(ledPin1, b.LOW);
    b.digitalWrite(ledPin2, b.LOW);
    b.digitalWrite(ledPin3, b.LOW);
    arrayY.shift();                   //Aqui actualizamos el vector
    arrayY.push(Math.round(x.value * 100)); 
    console.log(x.value * 100);
    if(x.value <= (33.33/100)){
        b.digitalWrite(ledPin3, b.HIGH);
    }
    if(x.value > (33.33/100) && x.value <= (66.66/100)){
        b.digitalWrite(ledPin2, b.HIGH);
    }
    if(x.value > (66.66/100)){
        b.digitalWrite(ledPin1, b.HIGH);
    }
  }


  let gaugeOptions = {

    chart: {
      type: 'solidgauge'
    },
  
    title: null,
  
    pane: {
      center: ['50%', '85%'],
      size: '140%',
      startAngle: -90,
      endAngle: 90,
      background: {
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
        innerRadius: '60%',
        outerRadius: '100%',
        shape: 'arc'
      }
    },
  
    tooltip: {
      enabled: false
    },
  
    // the value axis
    yAxis: {
      stops: [
        [0.1, '#DF5353'], // green
        [0.5, '#DDDF0D'], // yellow
        [0.9, '#55BF3B'] // red
      ],
      lineWidth: 0,
      minorTickInterval: null,
      tickAmount: 2,
      title: {
        y: -70
      },
      labels: {
        y: 16
      }
    },
  
    plotOptions: {
      solidgauge: {
        dataLabels: {
          y: 5,
          borderWidth: 0,
          useHTML: true
        }
      }
    }
  };
  
  // The potenciometro gauge
  var chartSpeed = Highcharts.chart('container', Highcharts.merge(gaugeOptions, {
    yAxis: {
      min: 0,
      max: 100,
      title: {
        text: 'Potenciómetro'
      }
    },
  
    credits: {
      enabled: false
    },
  
    series: [{
      name: 'Potenciómetro',
      data: [0],
      dataLabels: {
        format: '<div style="text-align:center"><span style="font-size:25px;color:' +
          ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
             '<span style="font-size:12px;color:silver">%</span></div>'
      },
      tooltip: {
        valueSuffix: ' %'
      }
    }]
  
  }));
  
  
  // Bring life to the dials
  setInterval(function () {
    // Speed
    var point,
      newVal,
      point = chartSpeed.series[0].points[0];
      if (arrayY != 100){
      newVal = arrayY;
      }
  
      point.update(newVal);
    
  }, 100);
  