//http://192.168.7.2/user/TFG/Planta.html

setTargetAddress(location.host, {
    initialized: run
    }); //Apuntamos a la direccion y ejecutamos en funcion run
  
  let b = undefined;


  let PWMPin = "P9_14";  // PWM Que controla el Nicrom
  let PWM2Pin = "P9_16"; //PWM Que controla el Ventilador
  let sumatorio = 0;     //Variable que se utiliza para hacer la media de 20 medidas por seg
  let count = 0;          
  let INAPin = "P9_36";  //Pin de lectura del Sensor PT100
  let NicromVoltage = 0.1; //Duty Cicle para el PWM del Nicrom (0-1)
  let FanVoltage = 0.1; //Duty Cicle para el PWM del Ventilador (0-1)
  let T;                //lectura de temperatura
  let tiempo = 0;
  let inicio = 0;  

  //Vectores para la construcción de las gráficas 

  //Vector que almacena los últimos 500 valores de tiempo 
  let tiempoV = 
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
    
];

  //Vector que almacena los últimos 500 valores de T 
  let resultadosTemperatura = 
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
    
]; //Vector necesario para construir la grafica
  
  //Vector que almacena los últimos 500 valores de NicromVoltage 
  let resultadosNicromVoltage = 
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,]; //Vector necesario para construir la grafica
  
  //Vector que almacena los últimos 500 valores de FanVoltage 
  let resultadosFanVoltage = 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,];


  function run(){
    b = myrequire('bonescript'); //libreria Bonescript
  }



  function actualiza(){ //Actualiza los valores del ventilador y del nicrom
    let a = document.querySelector(".Fan .faninput").value;
    if((a <= 1)&&(a >= 0)){
      FanVoltage = a;
    }
    let k =  document.querySelector(".Nicrom .nicrominput").value;
    if((k <= 1)&&(k >= 0)){
      NicromVoltage = k;
    }
    PWMWrite();
  }

  function PWMWrite(){ //Envía las señales PWM al Ventilador y al Nicrom
    b.analogWrite(PWMPin, FanVoltage, 490);
    b.analogWrite(PWM2Pin, NicromVoltage, 490);
    if(inicio == 0){
        INARead();
        inicio = 1;
        }
        return(inicio);
  }

  function INARead(){ //Realiza la lectura del sensor cada 50 ms
    b.analogRead(INAPin, Vo);
    setTimeout(INARead, 50)
  }

  function Vo(x){ //Almacena valores del sensor en sumatorio 
    if (count == 20){ //Al llegar a 20 significa que ha pasado 1 seg y pasa las 20 muestras sumadas
     count = 0;
     show(sumatorio);
     sumatorio = 0;
   }else{
     count++;
     sumatorio += x.value*1.8; // 1.8 por ser el voltaje
   }  
   }

  function show(sumatorio){  //Muestra los valores de salida de Vo (Salida del INA)
                            //Vs(Salida del puente de Wheastone)
                            //T Temperatura
                            
    let Vo = (sumatorio / 20); //Media de las medidas recogidas por el sensor (20 por seg)
    let Vs;

        Vs = Vo/29.0296296296296; //Ganancia del INA (Amplificador de Instrumentación)

      T = ((Vs*(100 + 1)*(100 + 1))/(12*100*0.00385));
      //T = (VS*(k+1)*(k+1))/(Vin*k*alpha);
      
      //Actualización del vector de Temperatura (Para el gráfico)
      resultadosTemperatura.shift();
      resultadosTemperatura.push(T);

      //Actualización del vector del tiempo (Para el gráfico)
      tiempoV.shift();
      tiempoV.push(tiempo);

      //Actualización del vector del Voltaje del Ventilador (Para el gráfico)
      resultadosFanVoltage.shift();
      resultadosFanVoltage.push(FanVoltage);

      //Actualización del vector del Voltaje del Ventilador (Para el gráfico)
      resultadosNicromVoltage.shift();
      resultadosNicromVoltage.push(NicromVoltage);

      tiempo++; //Aumenta el tiempo en 1, ya que la función se ejecuta cada seg

      console.log("Vo = " + Vo.toPrecision(4) + " V, Vs = " + (1000*Vs).toPrecision(4) + 
      " mV, T = " + T.toPrecision(3) + " ºC");

  }

  //Gráfico Tiempo-Temperatura
  Highcharts.chart('container1', {
    chart: {
      type: 'spline',
      animation: Highcharts.svg, // don't animate in old IE (NO INTERNET EXPLORER!!)
      marginRight: 10,
      events: {
        load: function () {
    
          // Funcion que actualiza el grafico a cada segundo
          var series = this.series[0];
          setInterval(function () {
            var x = tiempoV[499],// Pasamos la ultima posicion del vector (la mas actualizada) 
              y = parseFloat(resultadosTemperatura[499]);
            series.addPoint([x, y], true, true);
          }, 1000);
        }
      }
    },
    
    time: {
      useUTC: false
    },
    
    title: {
      text: 'PT100'
    },
    xAxis: {
      tickPixelInterval: 150,
      text: 'Segundos'
    },
    yAxis: {
      title: {
        text: 'ºC'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }]
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br/>',
      pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: true
    },
    series: [{
      name: 'Temperatura',
      data: (function () {
        // Generamos un vector que vaya guardando los datos anteriores
        let data = [];

        for (i = -499; i <= 0; i += 1) {
          data.push({
            x: tiempoV[i + 499],
            y: resultadosTemperatura[i + 499]
          });
        }
        return data;
      }())
    }]
    });
  
  //Gráfico Tiempo-VoltajeNicrom
  Highcharts.chart('container2', {
                chart: {
                  type: 'spline',
                  animation: Highcharts.svg, // don't animate in old IE (NO INTERNET EXPLORER!!)
                  marginRight: 10,
                  events: {
                    load: function () {
                
                      // Funcion que actualiza el grafico a cada segundo
                      var series = this.series[0];
                      setInterval(function () {
                        var x = tiempoV[499],// Pasamos la ultima posicion del vector (la mas actualizada)
                          y = parseFloat(resultadosNicromVoltage[499]);               
                        series.addPoint([x, y], true, true);
                      }, 1000);
                    }
                  }
                },
                
                time: {
                  useUTC: false
                },
                
                title: {
                  text: 'PWM Nicrom'
                },
                xAxis: {
                  tickPixelInterval: 150,
                  text: 'Segundos'
                },
                yAxis: {
                  title: {
                    text: 'Duty Cicle'
                  },
                  plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                  }]
                },
                tooltip: {
                  headerFormat: '<b>{series.name}</b><br/>',
                  pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
                },
                legend: {
                  enabled: false
                },
                exporting: {
                  enabled: true
                },
                series: [{
                  color: 'red',
                  name: 'PWMNicrom',
                  data: (function () {
                    // Generamos un vector que vaya guardando los datos anteriores
                    let data = [];
            
                    for (i = -499; i <= 0; i += 1) {
                      data.push({
                        x: tiempoV[i + 499],
                        y: resultadosNicromVoltage[i + 499]
                      });
                    }
                    return data;
                  }())
                }]
                });

  //Gráfico Tiempo-VoltajeVentilador          
  Highcharts.chart('container3', {
                  chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE (NO INTERNET EXPLORER!!)
                    marginRight: 10,
                    events: {
                      load: function () {
                  
                        // Funcion que actualiza el grafico a cada segundo
                        var series = this.series[0];
                        setInterval(function () {
                          var x = tiempoV[499],// Pasamos la ultima posicion del vector (la mas actualizada) 
                            y = parseFloat(resultadosFanVoltage[499]);
                          series.addPoint([x, y], true, true);
                        }, 1000);
                      }
                    }
                  },
                  
                  time: {
                    useUTC: false
                  },
                  
                  title: {
                    text: 'PWM Fan'
                  },
                  xAxis: {
                    tickPixelInterval: 150,
                    text: 'Segundos'
                  },
                  yAxis: {
                    title: {
                      text: 'Duty Cicle'
                    },
                    plotLines: [{
                      value: 0,
                      width: 1,
                      color: '#808080'
                    }]
                  },
                  tooltip: {
                    headerFormat: '<b>{series.name}</b><br/>',
                    pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
                  },
                  legend: {
                    enabled: false
                  },
                  exporting: {
                    enabled: true
                  },
                  series: [{
                    color: 'green',
                    name: 'PWMFan',
                    data: (function () {
                      // Generamos un vector que vaya guardando los datos anteriores
                      let data = [];
              
                      for (i = -499; i <= 0; i += 1) {
                        data.push({
                          x: tiempoV[i + 499],
                          y: resultadosFanVoltage[i + 499]
                        });
                      }
                      return data;
                    }())
                  }]
                  });