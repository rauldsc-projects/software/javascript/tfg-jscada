const int sensorPin = A2;    // seleccionar la entrada para el sensor
float sensorValue;       // variable que almacena el valor raw (0 a 1023)
float VoltajeINA;    
float T = 0; // Temperatura  
float alpha = 0.00385;// constante alpha de la pt100
float gananciaINA = 109.851177;
float VS=0 ;
float Vin = 12;
float k = 100;




float VoltajeTemp [100]; // Buffer que almacenará los 100 ultimos valores de temperatura en voltaje      &
int indexT; // Posición que se moverá en el bucle
int ActivarTemp = 0; // Activa la lectura de la temperatura
long previousMillis = 0;   // almacenará el último cambio
long interval = 500;   // intervalo de envío de temperatura(en milisegundos)              &


void setup() {
  // initialize serial communication with computer:
  Serial.begin(115200);
}

void loop() {
     sensorValue = analogRead(sensorPin);
     VoltajeINA = ((sensorValue/ 1023) * 5);

    VoltajeTemp [indexT] = VoltajeINA;
    indexT = ++indexT % 100;                                                // &

    float MediaVoltajeTemp = 0;
    for (int i = 0 ; i < 100 ; i++)                                              // &
      MediaVoltajeTemp = MediaVoltajeTemp + VoltajeTemp[i] ;

    MediaVoltajeTemp = MediaVoltajeTemp / 100;  // Esto sirve para promediar la muestra de temperatura en 100 muestras             &
    VS = (MediaVoltajeTemp/gananciaINA)+(0.681818/1000);    // el 0.681818 es un offset calculado con diferentes valores de temperatura
    T = (VS*(k+1)*(k+1))/(Vin*k*alpha);

   unsigned long currentMillis = millis(); // tiempo actual
   
    if (currentMillis - previousMillis > interval) { // si el tiempo actual - el anterior es menor que el intervalo entra aqui
      previousMillis = currentMillis;
      Serial.println("");
      Serial.print("Vo(V): ");
      Serial.print(MediaVoltajeTemp,3);
      Serial.print("    VS(mV): ");
      Serial.print(VS*1000,5);
      Serial.print("    Temperatura: ");
      Serial.println( T,1 ) ;
    }
    
 

    
    }
