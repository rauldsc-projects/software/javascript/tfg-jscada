
const int PinEntradaVentilador = A0;   // seleccionar la entrada para el sensor
int ValorEntradaVentilador;         // variable que almacena el valor raw (0 a 1023)
float VoltajeVentilador;            // variable que almacena el voltaje (0.0 a 25.0)

const int PinEntradaNicrom = A1;   // seleccionar la entrada para el sensor
int ValorEntradaNicrom;         // variable que almacena el valor raw (0 a 1023)
float VoltajeNicrom;            // variable que almacena el voltaje (0.0 a 25.0)

int PinConmutador =8 ;
int ValorConmutador = 0;

int valorPWMVentilador=0; // Debe ser entre 0 y 255
long valorSerialVentilador=80;
int VentiladorPWM = 9;

int valorPWMNicrom=0; // Debe ser entre 0 y 255
long valorSerialNicrom=0;
int NicromPWM = 10;

String cadena; // Lee desde el USB el Labview
String numcadena [50]; // Array que almacena los datos que les llega desde el Labview
int posInicio = 0; // Posiciones para la lectura de la cadena que llega desde serial, supongamos v43q , v sería posInicio y posFinal q
int posFinal; // posicion final


 
void setup() {
  Serial.begin(115200);
}
 
void loop() {
   ValorConmutador = digitalRead(PinConmutador);
   
   if(ValorConmutador == LOW){
    if (Serial.available() > 0) {
       cadena = Serial.readString();  // Leemos la cadena por serial

       int j = 0;
       for (int i = 0 ; i < cadena.length() ; i++) {      // Recorremos las posiciones de la cadena

           switch (cadena[i]) {
            
            case 'v': // Entre v y V estará la velocidad del ventilador por serial
              posInicio = i + 1;
              break;
            case 'V': 
              posFinal = i;                       // La posicion final será lo que recorra el bucle
              numcadena[j] = cadena.substring(posInicio, posFinal);  // Numcadena almacena en un string lo que va despues de v, PE(v43q) pues coge el 43
              valorSerialVentilador = numcadena[j].toInt();  // El valor del ventilador será el valor anterior convertido en entero
              j++;
              posInicio = i + 1;
              break;

            case 'n': // Entre n y N estará la intensidad del nicrom por serial
              posInicio = i + 1;
              break;
            case 'N': 
              posFinal = i;                       // La posicion final será lo que recorra el bucle
              numcadena[j] = cadena.substring(posInicio, posFinal);  // Numcadena almacena en un string lo que va despues de v, PE(v43q) pues coge el 43
              valorSerialNicrom = numcadena[j].toInt();  // El valor del Nicrom será el valor anterior convertido en entero
              j++;
              posInicio = i + 1;
              break;
            
          }
       }
    }


// Condiciones del ventilador serial

     if(valorSerialVentilador > 40 && valorSerialVentilador < 100){
       valorPWMVentilador = (255*valorSerialVentilador)/100; // Regla de tres para pasar el porcentaje al rango 0-255
    }
    if(valorSerialVentilador <= 40){
       valorPWMVentilador = (255*40)/100; 
    }

    if(valorSerialVentilador >= 100){
       valorPWMVentilador = (255*100)/100; 
    }
  
     analogWrite(VentiladorPWM, valorPWMVentilador);       // Escribir en el pin analógico el valor PWM


// Condicionces del nicrom serial

    if(valorSerialNicrom >= 0 && valorSerialVentilador < 85){
       valorPWMNicrom= (255*valorSerialNicrom)/100; // Regla de tres para pasar el porcentaje al rango 0-255
    }
       
    if(valorSerialNicrom >= 85){                         // Establecemos límite del Nicrom
       valorPWMNicrom = (255*valorSerialNicrom)/100; 
    }

    analogWrite(NicromPWM, valorPWMNicrom);       // Escribir en el pin analógico el valor PWM

} 

   if (ValorConmutador == HIGH){
   ValorEntradaVentilador = analogRead(PinEntradaVentilador);          // realizar la lectura
   VoltajeVentilador = fmap(ValorEntradaVentilador, 0, 1023, 0.0, 25.0);   // cambiar escala a 0.0 - 25.0
   VoltajeVentilador = VoltajeVentilador + 0.037; // calibración con respecto al tester
   if(VoltajeVentilador > 2 && VoltajeVentilador < 5){
       valorPWMVentilador = (255*VoltajeVentilador)/5; // Regla de tres para pasar el porcentaje al rango 0-255
    }
   if(VoltajeVentilador <= 2){
       valorPWMVentilador = (255*2)/5; 
    }

   if(VoltajeVentilador >= 5){
       valorPWMVentilador = (255*5)/5; 
    }
  
  analogWrite(VentiladorPWM, valorPWMVentilador);       // Escribir en el pin analógico el valor PWM
   
   ValorEntradaNicrom = analogRead(PinEntradaNicrom);          // realizar la lectura
   VoltajeNicrom = fmap(ValorEntradaNicrom, 0, 1023, 0.0, 25.0);   // cambiar escala a 0.0 - 25.0
   VoltajeNicrom = VoltajeNicrom + 0.037; // calibración con respecto al tester
   if(VoltajeNicrom >= 0 && VoltajeNicrom < 4.25){
       valorPWMNicrom= (255*VoltajeNicrom)/5; // Regla de tres para pasar el porcentaje al rango 0-255
    }
   if(VoltajeNicrom >= 4.25){
       valorPWMNicrom = (255*4.25)/5; 
    }
    
  analogWrite(NicromPWM, valorPWMNicrom);       // Escribir en el pin analógico el valor PWM

    Serial.println(VoltajeNicrom,3);                     // mostrar el valor por serial

   delay(1000);

   }

  
   
}
 
// cambio de escala entre floats
float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}